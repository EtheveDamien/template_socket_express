# Express.js and Socket.io template

This project is a minimalist template using express and socketio.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Just clone this project.

```
git clone git@gitlab.com:EtheveDamien/template_socket_express.git
```

or

```
git clone https://gitlab.com/EtheveDamien/template_socket_express.git
```

### Installing

Installing dependencies and run the project.

```
npm i
npm start
```

## Authors

* **Damien ETHÈVE** - *Initial work* - [gitlab](https://gitlab.com/EtheveDamien)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
