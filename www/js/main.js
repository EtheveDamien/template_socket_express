window.onload = () => {
  const io_client = io("http://localhost:3000");

  io_client.on("connect", ()=> {
    console.log("connected to server");
  });

  io_client.on("disconnect", ()=> {
    console.log("disconnected from server");
  });
}
